//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "GrafUnit.h"
#include "AboutUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "PERFGRAP"
#pragma resource "*.dfm"

#define max(a, b)  (((a) > (b)) ? (a) : (b))

 // определение цветовой палитры для графа
 const TColor GrafPoint_Color = clRed;    // цвет вершины
 const TColor GrafArc_Color   = clGreen;  // цвет дуги
 const TColor GrafArrow_Color = clMaroon; // цвет стрелки
 const TColor GrafLoop_Color  = clBlue;   // цвет петли

TForm_Graf *Form_Graf;
//---------------------------------------------------------------------------
__fastcall TForm_Graf::TForm_Graf(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::FormActivate(TObject *Sender)
{ // активизация формы

  Panel_Analitic->Height = 0; // теперь панель невидима
  Panel_Matrix->Height   = 0; // теперь панель невидима

  Image->Canvas->Brush->Color = clInfoBk;
  Image->Canvas->TextFlags = ETO_OPAQUE;

  // очистка графического представления графа
  Image->Canvas->FillRect(Image->ClientRect);
  Image->Canvas->Refresh();

  Start = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::ComboBox_GrafKindChange(TObject *Sender)
{ // выбор вида графа

  ComboBox_GrafKind->Enabled = false; // теперь выбор вида графа недоступен

  switch (ComboBox_GrafKind->ItemIndex) {
   case 0: // неориентированный граф
    StatusBar1->Panels->Items[5]->Text = "Изображается неориентированный граф ...";
    Application->ProcessMessages();
     Sleep (2000);
    StatusBar1->Panels->Items[5]->Text = "";
    break;
   case 1: // ориентированный граф
    StatusBar1->Panels->Items[5]->Text = "Изображается ориентированный граф ...";
    Application->ProcessMessages();
     Sleep (2000);
    StatusBar1->Panels->Items[5]->Text = "";
    break;
  }

  // очистка графического представления графа
  Image->Canvas->FillRect(Image->ClientRect);
  Image->Canvas->Refresh();

  StatusBar1->Panels->Items[4]->Text = "Черчение графа ...";
  Image->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::GrafPointSearch (int *A, int *B, int *C)
{ // поиск указанной вершины графа
  int SquareRadius;

  StatusBar1->Panels->Items[4]->Text = "Черчение графа ...";
  StatusBar1->Panels->Items[5]->Text = "";
  for (int i=1;  i <= GrafQuantityItems;  i++) {
   SquareRadius = (MyGraf[i].FromX - MouseX)*(MyGraf[i].FromX - MouseX) +
                  (MyGraf[i].FromY - MouseY)*(MyGraf[i].FromY - MouseY);
   if (SquareRadius < 100) {
    // вершина найдена
    *A = MyGraf [i].FromX;
    *B = MyGraf [i].FromY;
    *C = MyGraf [i].FromNumber;

    MouseX = *A;
    MouseY = *B;

    StatusBar1->Panels->Items[4]->Text = "Захват цели ...";
    StatusBar1->Panels->Items[5]->Text = "Цель захвачена!";
    Application->ProcessMessages();
    return;
   }

   SquareRadius = (MyGraf[i].ToX - MouseX)*(MyGraf[i].ToX - MouseX) +
                  (MyGraf[i].ToY - MouseY)*(MyGraf[i].ToY - MouseY);
   if (SquareRadius < 100) {
    // вершина найдена
    *A = MyGraf [i].ToX;
    *B = MyGraf [i].ToY;
    MouseX = *A;
    MouseY = *B;
    *C = MyGraf [i].ToNumber;

    StatusBar1->Panels->Items[4]->Text = "Захват цели ...";
    StatusBar1->Panels->Items[5]->Text = "Цель захвачена!";
    Application->ProcessMessages();
    return;
   }
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::ImageMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{ // отображение координат мыши родительским контейнером при её перемещении

  StatusBar1->Panels->Items[1]->Text = AnsiString (X);
  StatusBar1->Panels->Items[3]->Text = AnsiString (Y);

  MouseX = X;
  MouseY = Y;

  A = B = C = -1;
  // поиск указанной точки среди вершин графа и захват цели
  GrafPointSearch (&A, &B, &C);
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::ImageMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{ // запоминание координат курсора мыши при рисовании графа

  bool TargetLocate = (A == -1) ? false : true;
  int LocateMouseX,
      LocateMouseY;

  Panel_Analitic->Height = 0; // теперь панель невидима
  Panel_Matrix->Height   = 0; // теперь панель невидима

  if (ComboBox_GrafKind->ItemIndex == -1) { // вид графа не выбран
   ShowMessage ("Выберите вид графа!");
   return;
  }

  if (!TargetLocate) { // цель не захвачена
   if (GrafQuantityItems == 10) {
    ShowMessage ("Граф не может иметь более 10 вершин!");
    return;
   }

   LocateMouseX = X; // текущие координаты мыши
   LocateMouseY = Y;

   // кнопка стирания графа доступна
   BitBtn_Clear->Enabled = true;
   // кнопка создания матриц графа доступна
   BitBtn_Matrix->Enabled = true;
   // кнопка создания аналитического описания графа доступна
   BitBtn_Analitic->Enabled = true;
  } else { // цель захвачена
     LocateMouseX = MouseX; // координаты цели
     LocateMouseY = MouseY;
    }

  if (Start) { // начало дуги графа
   ++GrafQuantityItems; // номер дуги графа

   // значения по-умолчанию
   MyGraf[GrafQuantityItems].Loop = false; // это петля?
   MyGraf[GrafQuantityItems].Orientation = false; // ребро ориентировано?

   // запоминание начала дуги графа
   MyGraf[GrafQuantityItems].FromX = LocateMouseX;
   MyGraf[GrafQuantityItems].ToX   = LocateMouseX; // координата Х конца ребра графа
   MyGraf[GrafQuantityItems].FromY = LocateMouseY;
   MyGraf[GrafQuantityItems].ToY   = LocateMouseY; // координата Y конца ребра графа

   // GrafCurrentItem - номер текущей вершины графа
   FromPointNumber = (TargetLocate) /* вершина найдена */ ? C : ++GrafCurrentItem;
   MyGraf[GrafQuantityItems].FromNumber = FromPointNumber;
   MyGraf[GrafQuantityItems].ToNumber   = FromPointNumber; // номер вершины конца ребра графа

   Image->Canvas->MoveTo (LocateMouseX, LocateMouseY); // перемещение в точку (X,Y)
   Image->Canvas->Refresh();
   StartX = LocateMouseX;
   StartY = LocateMouseY;

   if (!TargetLocate) {
    // изображение начала новой дуги графа
    Image->Canvas->Pen->Color = GrafPoint_Color;
    Image->Canvas->Ellipse(LocateMouseX-2, LocateMouseY-2,
                           LocateMouseX+2, LocateMouseY+2);
    Image->Canvas->Refresh();
     Image->Canvas->TextOut(LocateMouseX, LocateMouseY, AnsiString (GrafCurrentItem));
   }
  } else { // конец дуги графа
     // запоминание конца дуги графа
     MyGraf[GrafQuantityItems].ToX = LocateMouseX;
     MyGraf[GrafQuantityItems].ToY = LocateMouseY;
     ToPointNumber = (TargetLocate) /* вершина найдена */ ? C : ++GrafCurrentItem;
     MyGraf[GrafQuantityItems].ToNumber = ToPointNumber;

     for (int j=1;   j<= GrafQuantityItems - 1;   j++ ) {
      if (MyGraf[GrafQuantityItems].FromX == MyGraf[j].FromX &&
          MyGraf[GrafQuantityItems].FromY == MyGraf[j].FromY &&
          MyGraf[GrafQuantityItems].ToX == MyGraf[j].ToX &&
          MyGraf[GrafQuantityItems].ToY == MyGraf[j].ToY) {
       // ошибка "лишние" вершины графа
       ShowMessage ("Ошибка! Лишние вершины графа.");
       --GrafQuantityItems; // номер дуги графа
       Start = true;
       return;
      }
     }

     if (MyGraf[GrafQuantityItems].ToX == MyGraf[GrafQuantityItems].FromX &&
         MyGraf[GrafQuantityItems].ToY == MyGraf[GrafQuantityItems].FromY) {
      // вершины графа совпадают -> это петля
      MyGraf[GrafQuantityItems].Loop = true;
      // вычерчивание петли
      Image->Canvas->Pen->Color = GrafLoop_Color;
      Image->Canvas->Brush->Style = bsClear;
      Image->Canvas->Ellipse(LocateMouseX-20, LocateMouseY-20,
                             LocateMouseX+20, LocateMouseY+20);
      Image->Canvas->Brush->Style = bsSolid;
     } else { // это не петля
        MyGraf[GrafQuantityItems].Loop = false;

        if (!TargetLocate) {
         // изображение конца новой дуги графа
         Image->Canvas->Pen->Color = GrafPoint_Color;
         Image->Canvas->Ellipse(LocateMouseX-2, LocateMouseY-2,
                                LocateMouseX+2, LocateMouseY+2);
         Image->Canvas->Refresh();
         Image->Canvas->TextOut(LocateMouseX, LocateMouseY, AnsiString (GrafCurrentItem));
        }
        // вычерчивание отрезка прямой до точки (X,Y)
        Image->Canvas->Pen->Color = GrafArc_Color;
        Image->Canvas->MoveTo (StartX, StartY);
        Image->Canvas->LineTo (LocateMouseX, LocateMouseY);
        Image->Canvas->Refresh();

        if (ComboBox_GrafKind->ItemIndex == 1) { // для ориентированного графа
         // вычерчивание стрелки на конце дуги графа
         DrawArrow ();
         MyGraf[GrafQuantityItems].Orientation = true;
        } else { // ребро не ориентировано
           MyGraf[GrafQuantityItems].Orientation = false;
          }
       } // это не петля
    } // конец дуги графа

  Start = !Start;
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::DrawArrow (void)
{ // вычерчивание стрелки

  int dX, dY, Arrow_X, Arrow_Y;
  long double ArrowAngle;

  Image->Canvas->Pen->Color = GrafArrow_Color;

  dX = MouseX - StartX;
  dY = MouseY - StartY;

  ArrowAngle = M_PI - (atan2 (dY, dX) - 0.2);
  Arrow_X = int (MouseX + 10 * cos (ArrowAngle));
  Arrow_Y = int (MouseY - 10 * sin (ArrowAngle));
  Image->Canvas->MoveTo (Arrow_X, Arrow_Y);
  Image->Canvas->LineTo (MouseX, MouseY);

  ArrowAngle = M_PI - (atan2 (dY, dX) + 0.2);
  Arrow_X = int (MouseX + 10 * cos (ArrowAngle));
  Arrow_Y = int (MouseY - 10 * sin (ArrowAngle));
  Image->Canvas->MoveTo (Arrow_X, Arrow_Y);
  Image->Canvas->LineTo (MouseX, MouseY);

  Image->Canvas->Refresh();
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::BitBtn_ClearClick(TObject *Sender)
{ // стирание графа

  // очистка аналитического представления графа
  // очистка матричного представления графа

  Image->Enabled = false;

  // очистка графического представления графа
  Image->Canvas->FillRect(Image->ClientRect);
  Image->Canvas->Refresh();

  // сброс параметров графа
  GrafQuantityItems = 0;
  GrafCurrentItem = 0;

  // кнопка стирания графа недоступна
  BitBtn_Clear->Enabled = false;
  // кнопка создания матриц графа недоступна
  BitBtn_Matrix->Enabled = false;
  // кнопка создания аналитического описания графа недоступна
  BitBtn_Analitic->Enabled = false;

  StatusBar1->Panels->Items[5]->Text = "Граф стёрт!";
  Application->ProcessMessages();
   Sleep (2000);
  StatusBar1->Panels->Items[5]->Text = "";

  ComboBox_GrafKind->ItemIndex = -1;
  ComboBox_GrafKind->Text = "выберите вид графа";
  ComboBox_GrafKind->Enabled = true;

  Panel_Analitic->Height = 0; // теперь панель невидима
  Panel_Matrix->Height   = 0; // теперь панель невидима
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::BitBtn_MatrixClick(TObject *Sender)
{ // создание матричного описания графа

  if (GrafCurrentItem < 2) {
   ShowMessage ("Граф имеет только одну вершину."
                " Построение матричного описания графа невозможно!");
   return;
  }

  // переопределение размеров матрицы смежности
  StringGrid_MatrixContiguity->ColCount = GrafCurrentItem + 1;
  StringGrid_MatrixContiguity->RowCount = GrafCurrentItem + 1;

  // формирование геометрии матрицы смежности
  for (int i=0;   i < StringGrid_MatrixContiguity->ColCount;   i++) {
   StringGrid_MatrixContiguity->ColWidths[i] = 34;
   StringGrid_MatrixContiguity->RowHeights[i] = 13;
   // заполнение титулов строк и столбцов
   if (i) StringGrid_MatrixContiguity->Cells[0][i] = i;
   if (i) StringGrid_MatrixContiguity->Cells[i][0] = i;
  }

  // заполнение матрицы смежности нулями
  for (int Col=1;   Col < StringGrid_MatrixContiguity->ColCount;   Col++)
   for (int Row=1;   Row < StringGrid_MatrixContiguity->ColCount;   Row++)
    StringGrid_MatrixContiguity->Cells[Col][Row] = "0";

  // переопределение размеров матрицы инциденций
  StringGrid_MatrixIncidence->ColCount = GrafCurrentItem   + 1;
  StringGrid_MatrixIncidence->RowCount = GrafQuantityItems + 1;

  // формирование геометрии матрицы инциденций
  for (int i=0;   i < StringGrid_MatrixIncidence->ColCount;   i++) {
   StringGrid_MatrixIncidence->ColWidths[i] = 34;
   // заполнение титулов столбцов
   if (i) StringGrid_MatrixIncidence->Cells[i][0] = i;
  }
  for (int i=0;   i < StringGrid_MatrixIncidence->RowCount;   i++) {
   StringGrid_MatrixIncidence->RowHeights[i] = 13;
   // заполнение титулов строк
   if (i) StringGrid_MatrixIncidence->Cells[0][i] = i;
  }

  // заполнение матрицы инциденций нулями
  for (int Col=1;   Col < StringGrid_MatrixIncidence->ColCount;   Col++)
   for (int Row=1;   Row < StringGrid_MatrixIncidence->RowCount;   Row++)
    StringGrid_MatrixIncidence->Cells[Col][Row] = "0";

  for (int i=1;   i <= GrafQuantityItems;   i++) {
   // заполнение матрицы смежности данными из графа
   if (StringGrid_MatrixContiguity->Cells[MyGraf[i].ToNumber][MyGraf[i].FromNumber] == "0") {
    // отображение координат очередной вершины графа в матрице смежности
    StringGrid_MatrixContiguity->Cells[MyGraf[i].ToNumber][MyGraf[i].FromNumber] = "1";
    StringGrid_MatrixContiguity->Cells[MyGraf[i].FromNumber][MyGraf[i].ToNumber] = "1";
   } else
      if (StringGrid_MatrixContiguity->Cells[MyGraf[i].ToNumber][MyGraf[i].FromNumber] == "1") {
       // отображение координат очередной вершины графа в матрице смежности
       StringGrid_MatrixContiguity->Cells[MyGraf[i].ToNumber][MyGraf[i].FromNumber] = "2";
       StringGrid_MatrixContiguity->Cells[MyGraf[i].FromNumber][MyGraf[i].ToNumber] = "2";
      } else {
         // ошибка "лишние" вершины графа
         ShowMessage ("Ошибка! Лишние вершины графа.");
         Image->Canvas->MoveTo (StartX, StartY); // перемещение в точку (X,Y)
         Start = true;
        }

   // заполнение матрицы инциденций данными из графа
   StringGrid_MatrixIncidence->Cells[MyGraf[i].ToNumber][i]   = "1";
   StringGrid_MatrixIncidence->Cells[MyGraf[i].FromNumber][i] = "1";
  }

  Panel_Matrix->Height = /* теперь панель матриц видима */
   40 + StringGrid_MatrixContiguity->RowHeights[1] *
        (max(GrafCurrentItem,GrafQuantityItems) + 2);
}
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::BitBtn_AnaliticClick(TObject *Sender)
{ // создание аналитического описания графа

  if (GrafCurrentItem < 2) {
   ShowMessage ("Граф имеет только одну вершину."
                " Построение аналитического описания графа невозможно!");
   return;
  }

  AnsiString tmpString = "G=G(X,E); X={";

  for (int i=1;   i <= GrafCurrentItem;   i++) {
   tmpString += i;
   if (i != GrafCurrentItem) tmpString += ",";
 }
  tmpString += "};";

  tmpString += " E={";
  for (int i=1;   i <= GrafQuantityItems;   i++) {
   tmpString += "(";
   tmpString += MyGraf[i].FromNumber;
   tmpString += ",";
   tmpString += MyGraf[i].ToNumber;
   tmpString += ")";
   if (i != GrafQuantityItems) tmpString += ",";
  }
  tmpString += "};";

  Edit_Analitic->Text = tmpString;

  Panel_Analitic->Height = 40; // теперь панель видима
}
//---------------------------------------------------------------------------
/*
void __fastcall TForm_Graf::ToolButton_GrafEditClick(TObject *Sender)
{ // режим редактирования графа

  if (!ToolButton_GrafEdit->Down) { // режим редактирования отключен
   // формирование описаний графа
   BitBtn_AnaliticClick(this);
   BitBtn_MatrixClick(this);
  } else { // режим редактирования включен


     Panel_Analitic->Height = 0; // теперь панель невидима
     Panel_Matrix->Height   = 0; // теперь панель невидима
    }
}
*/
//---------------------------------------------------------------------------
void __fastcall TForm_Graf::BitBtn1Click(TObject *Sender)
{ // об авторе программы

  StatusBar1->Panels->Items[5]->Text = " Автор программы Бурса А.Г.";
  Application->ProcessMessages();
   Sleep (2000);
  StatusBar1->Panels->Items[5]->Text = "";
}
//---------------------------------------------------------------------------

