//---------------------------------------------------------------------------
#ifndef GrafUnitH
#define GrafUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Buttons.hpp>
#include "PERFGRAP.h"
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <Menus.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TForm_Graf : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel_Top;
        TPanel *Panel_Analitic;
        TPanel *Panel_Matrix;
        TStaticText *StaticText_Analitic;
        TStaticText *StaticText_MatrixCaption;
        TPanel *Panel_MatrixCaption;
        TStaticText *StaticText_MatrixIncidence;
        TStaticText *StaticText_MatrixContiguity;
        TStringGrid *StringGrid_MatrixIncidence;
        TStringGrid *StringGrid_MatrixContiguity;
        TPanel *Panel_Main;
        TToolBar *ToolBar1;
        TComboBox *ComboBox_GrafKind;
        TBitBtn *BitBtn_Clear;
        TToolButton *ToolButton1;
        TStatusBar *StatusBar1;
        TSplitter *Splitter1;
        TPanel *Panel_Graf;
        TBitBtn *BitBtn_Matrix;
        TEdit *Edit_Analitic;
        TBitBtn *BitBtn_Analitic;
        TImageList *ImageList1;
        TScrollBox *ScrollBox_Graf;
        TStaticText *StaticText_GrafCaption;
        TPanel *Panel_GrafImage;
        TImage *Image;
        TToolButton *ToolButton2;
        TToolButton *ToolButton3;
        TStaticText *StaticText1;
        TToolButton *ToolButton5;
        TBitBtn *BitBtn1;
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall ComboBox_GrafKindChange(TObject *Sender);
        void __fastcall BitBtn_ClearClick(TObject *Sender);
        void __fastcall ImageMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ImageMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall BitBtn_MatrixClick(TObject *Sender);
        void __fastcall BitBtn_AnaliticClick(TObject *Sender);
        void __fastcall BitBtn1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations

 int GrafQuantityItems, // количество вершин графа
     GrafCurrentItem;   // номер текущей вершины графа
 struct TMyGraf {
  int FromX;   // координата Х начала ребра графа
  int FromY;   // координата Y начала ребра графа
  int FromNumber;  // номер вершины начала ребра графа
  int ToX;   // координата Х конца ребра графа
  int ToY;   // координата Y конца ребра графа
  int ToNumber;  // номер вершины конца ребра графа
  bool Loop;    // это петля?
  bool Orientation; // ребро ориентировано?
 } MyGraf [11];  // максимум 10 рёбер

 // захваченная цель ...
 int A, B, C; // координаты вершины графа и номер ребра графа

 int MouseX, MouseY; // координаты мыши
 int TargetSize; // размер цели
 int FromPointNumber, ToPointNumber;

 bool Start;
 int StartX, StartY; // координаты начала дуги графа

 void __fastcall DrawArrow (void);
 void __fastcall GrafPointSearch (int *A, int *B, int *C);

        __fastcall TForm_Graf(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm_Graf *Form_Graf;
//---------------------------------------------------------------------------
#endif
